import { Injectable } from '@angular/core';

import {Employee} from "../models/Employee";
import {EMPLOYEES} from "../../assets/data/mock-employees";



@Injectable({
  providedIn: 'root'
})
export class DataService {


  constructor() { }

  // returns all of the Employees from the data directory inside of the assets directory
  getEmployees(): Employee[] {
    return EMPLOYEES;
  }


}
