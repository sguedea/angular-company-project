import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EmployeesComponent } from './components/employees/employees.component';
import {MatCardModule} from "@angular/material/card";

// service
import { DataService } from "./services/data.service";
// import { EmployeeDetailsComponent } from './components/employee-details/employee-details.component';
import {MatButtonModule} from "@angular/material/button";
import {MatExpansionModule} from "@angular/material/expansion";

@NgModule({
  declarations: [
    AppComponent,
    EmployeesComponent,
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        MatCardModule,
        HttpClientModule,
        MatButtonModule,
        MatExpansionModule
    ],
  providers: [
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
