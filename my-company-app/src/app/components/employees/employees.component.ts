import { Component, OnInit } from '@angular/core';
import { Employee } from "../../models/Employee";
import {DataService} from "../../services/data.service";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[];
  selectedEmployee: Employee;
  panelOpenState = true;


  // injection the service as the constructor's argument
  constructor(private dataService: DataService) { }

  ngOnInit(): void {

    // initialize your getEmployees() method

    this.getEmployees();
  }


  // assign your property to the getEmployees() method from your service
  getEmployees() {
    this.employees = this.dataService.getEmployees();
  }


  onSelect(employee: Employee) {
    this.selectedEmployee = employee;
  }



}
